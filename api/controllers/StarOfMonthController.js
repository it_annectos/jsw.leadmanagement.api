/**
 * StarOfMonthController
 *
 * @description :: Server-side logic for managing Starofmonths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 * created by sushant on 30-06-2018
 */

module.exports = {

  /*
    	#
    	Method: getStarOfMonth
    	# Description: to fetch StarOfMonth from database based on filter
    	*/
  getStarOfMonth: function (req, res) {
    console.log("comeing in the controller");
    var token = req.token;
    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Fetch StarOfMonth";
    userLogObject.eventType = "Get";

    StarOfMonth.getStarOfMonth(req.body, token, function (response) {
      res.json(response);
      console.log("comeing in output")
      userLogObject.response = response.message;
      LogService.addUserLog(userLogObject, token);
    });
  },
  /*
     	# Method: updateLead
     	# Description: to update Lead
     	*/
  addStarOfMonth: function (req, res) {
    var token = req.token;
    var programInfo = req.programObject;

    //Variable defined to create userLogObject
    var userLogObject = req.body.frontendUserInfo;
    userLogObject.requestApi = req.baseUrl + req.path;
    userLogObject.event = "Add StarOfMonth";
    userLogObject.eventType = "Add";
    userLogObject.template = req.body.template;
    console.log("Reachinghere", req.baseUrl + req.path);
    StarOfMonth.addStarOfMonth(req.body, programInfo, token, userLogObject, function (response) {
      res.json(response);
      //console.log("body is", req.body);
      userLogObject.response = response.message;
      userLogObject.responseData = response;
      LogService.addUserLog(userLogObject, token);
    });
  },

};
